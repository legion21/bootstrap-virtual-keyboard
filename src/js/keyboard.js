// Глобальная клавиатура и список подключенных объектов
//---------------------------------------------------
var _gledoms  = [];
//---------------------------------------------------
// Плагин виртуальной клавиатуры
(function ($) {
    // Класс виртуальной клавиатуры
    var Keyboard = function (opts) {
        //---------------------------------------------------
        // Переменные
        //---------------------------------------------------
        var self        = this;
        var activeDOM   = null;
        //---------------------------------------------------
        var $layout     = $("<div class='keyboard'/>");
        var enabled     = true;
        var options     = $.extend({
            type: "text", // text, symbols, numbers
            theme: "default",
            language: "eng",
            is_enable: true,
            is_hidden: true,
            animate_duration: 334
        }, opts || {});
        //---------------------------------------------------
        // Методы
        //---------------------------------------------------
        var initKeyboard = function () {
            $('body').append($layout);

            $layout.data('virtual-keyboard', self);

            updateLayer();

            if (options.is_hidden) {
                $layout.hide();
            }
            if (!options.is_enable) {
                $('div.keyboard button').attr('disabled', 'disabled');
            }
        };

        var hideKeyboard = function() {
            if (!$layout.is(':hidden')) {
                $layout.slideUp(options.animate_duration);
            }
        };

        var showKeyboard = function() {
            if ($layout.is(':hidden')) {
                $layout.slideDown(options.animate_duration);
            }
        };

        var updateLayer = function () {
            var template = templates[options.type || 'text'];

            $layout.html("<div class='"+(template.class || 'row')+"'><table class='keyboard-keys'/></div>");

            $.each(template[options.language || 'eng'],
            function (index, row) {
                $('.keyboard-keys').append('<tr class="keyboard-row" id="kr-'+index+'"></tr>');
                var size = 100 / row.length-1;
                $.each(row, function (key, val) {
                    if(val) {
                        var btnLayer = '<span>&ensp;</span>';
                        if(!val.skip) {
                            btnLayer = '<button class="btn btn-key'+(val.v.length > 1 ? '-command '+val.v.toLowerCase() : ' char')+'" data-value="'+val.v+'">' +
                                        (val.i ? '<i class="'+val.i+'"></i>' : (val.t || val.v)) +
                                        '</button>';
                        }
                        $('.keyboard-keys #kr-' + index).append('<td width="'+size+'%"' + (val.c ? ' colspan=' + val.c : '')+ '>'+btnLayer+'</td>')
                    }
                });
            });
            initCharActions();
        };

        var actionChar = function(action, char) {
            if(activeDOM)  {
                var start   = activeDOM.prop("selectionStart");
                var end     = activeDOM.prop("selectionEnd");
                var length  = activeDOM.val().length;
                var array   = activeDOM.val().split('');

                switch (action) {
                    case 'delete':
                        if (start < length) {
                            if (end - start == 0)
                                array.splice(start, 1);
                            else if (end - start > 0)
                                array.splice(start, end - start);
                            else return false;
                        }
                        break;

                    case 'backspace':
                        if (start >= 0) {
                            if (end - start == 0) {
                                array.splice(start - 1, 1);
                                start -= 1;
                            }
                            else if (end - start > 0)
                                array.splice(start, end - start);
                            else
                                return false;
                        }
                        break;

                    case 'enter':
                        if (activeDOM.is("textarea")) {
                            var a1 = array.slice(0, start).concat(String.fromCharCode(13, 10));
                            var a2 = array.slice(end, length);
                            array = a1.concat(a2);
                            start += 1;
                        }
                        break;

                    default:
                        var a01 = array.slice(0, start).concat(char);
                        var a02 = array.slice(end, length);
                        array = a01.concat(a02);
                        start += 1;
                        break;
                }

                var value = "";
                for (var i = 0; i < array.length; i++)
                    value = value.concat(array[i]);

                activeDOM.val(value);
                activeDOM.prop("selectionStart", start);
                activeDOM.prop("selectionEnd", start);
                activeDOM.focus();

                return true;
            }
            return false;
        };

        var initCharActions = function () {

            var capslock = false, $keyboard = $('div.keyboard');

            $keyboard.find('button').click(function () {
                var $this = $(this);
                var character = (''+$this.data('value')).toLowerCase();

                // Change Type Keyboard
                if ($this.hasClass('change')) {
                    switch (options.type) {
                        case 'text':
                            options.type = 'symbols';
                            break;

                        case 'symbols':
                            options.type = 'text';
                            break;

                        default: return;
                    }
                    updateLayer();
                    return;
                }

                // CapsLock
                if ($this.hasClass('capslock')) {

                    $('.char').toggleClass('uppercase');
                    capslock = true;

                    if (capslock)
                        $this.toggleClass('active');

                    return;
                }

                // BackSpace
                if ($this.hasClass('backspace')) {
                    actionChar('backspace');
                    return;
                }

                // Delete
                if ($this.hasClass('delete')) {
                    actionChar('delete');
                    return;
                }

                // Enter
                if ($this.hasClass('enter')) {
                    actionChar('enter');
                    return;
                }

                // Submit
                if ($this.hasClass('submit')) {
                    activeDOM = null;
                    hideKeyboard();
                    return;
                }

                // Uppercase letter
                if ($this.hasClass('uppercase'))
                    character = character.toUpperCase();


                // Space
                if ($this.hasClass('space')) {
                    actionChar('space', ' ');
                    return;
                }

                // Tab
                if ($this.hasClass('tab')) {
                    if(activeDOM) {
                        var pos = activeDOM.data('pos-index');
                        if(pos) {
                            if(pos >= 0) {
                                if (pos >= _gledoms.length) {
                                    pos = 0;
                                }
                                _gledoms[pos].focus();
                            }
                        }
                    }
                    return;
                }

                // Draw all other characters(letters, numbers and symbols)
                if ($this.hasClass('char')) {
                    actionChar('char', character);
                    return;
                }
                if(activeDOM) {
                    activeDOM.focus();
                }
            });
        };

        self.init = function(){
            initKeyboard();
        };

        self.setActiveDOM = function (element) {
            activeDOM = element;
        };

        self.setKeysEnable = function($value){
            if (!$value){
                $('div.keyboard button').attr('disabled', 'disabled');
                options.enabled = false;
            }
            else{
                $('div.keyboard button').removeAttr("disabled");
                options.enabled = true;
            }
        };

        self.setEnabled = function(value){
            enabled = value;
            if(enabled) {
                if(activeDOM) {
                    showKeyboard();
                }
            } else {
                hideKeyboard();
            }
        };

        self.isEnabled = function(){
            return enabled;
        };

        self.isKeysEnabled = function(){
            return options.is_enable;
        };

        self.setHidden = function(value){
            if (value){
                hideKeyboard();
            }  else{
                showKeyboard();
            }
        };

        self.isHidden = function(){
            return options.is_hidden;
        };

        // Шаблоны разметка
        //---------------------------------------------------
        var templates = {
            text: {
                class: 'col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1',
                eng: [
                    [
                        {v: 'TAB', t: null, i: 'glyphicon glyphicon-retweet', skip: null},
                        {v: 'A'},
                        {v: 'B'},
                        {v: 'C'},
                        {v: 'D'},
                        {v: 'E'},
                        {v: 'F'},
                        {v: 'G'},
                        {v: 'H'},
                        {v: 'I'},
                        {v: 'J'},
                        {v: 'K'},
                        {v: 'L'},
                        {v: 'M'},
                        {v: 'ENTER', i: 'glyphicon icon-enter'}
                    ],[
                        {v: 'CAPSLOCK', i: 'glyphicon icon-caps-lock'},
                        {v: 'N'},
                        {v: 'O'},
                        {v: 'P'},
                        {v: 'Q'},
                        {v: 'R'},
                        {v: 'S'},
                        {v: 'T'},
                        {v: 'U'},
                        {v: 'V'},
                        {v: 'W'},
                        {v: 'X'},
                        {v: 'Y'},
                        {v: 'Z'},
                        {v: 'SUBMIT', i: 'glyphicon icon-ok'}
                    ],[
                        {v: 'CHANGE', i: 'glyphicon icon-change'},
                        {v: '1'},
                        {v: '2'},
                        {v: '3'},
                        {v: '4'},
                        {v: '5'},
                        {v: '6'},
                        {v: '7'},
                        {v: '8'},
                        {v: '9'},
                        {v: '0'},
                        {v: 'SPACE', c: 3, i: 'glyphicon icon-space'},
                        {v: 'BACKSPACE', i: 'glyphicon icon-backspace'}
                    ]
                ]
            },
            symbols: {
                class: 'col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1',
                eng: [
                    [
                        {v: 'TAB', t: null, i: 'glyphicon glyphicon-retweet', skip: null},
                        {v: '~'},
                        {v: '!'},
                        {v: '@'},
                        {v: '#'},
                        {v: '$'},
                        {v: '%'},
                        {v: '^'},
                        {v: '&'},
                        {v: '*'},
                        {v: '('},
                        {v: ')'},
                        {v: '-'},
                        {v: '+'},
                        {v: 'ENTER', i: 'glyphicon icon-enter'}
                    ],[
                        {v: 'CAPSLOCK', skip: true, i: 'glyphicon icon-caps-lock'},
                        {v: '?'},
                        {v: '['},
                        {v: ']'},
                        {v: '{'},
                        {v: '}'},
                        {v: '<'},
                        {v: '>'},
                        {v: ';'},
                        {v: ':'},
                        {v: '\''},
                        {v: '"'},
                        {v: '_'},
                        {v: '='},
                        {v: 'SUBMIT', i: 'glyphicon icon-ok'}
                    ],[
                        {v: 'CHANGE', i: 'glyphicon icon-change'},
                        {v: '|'},
                        {v: '\\'},
                        {v: '/'},
                        {v: '±'},
                        {v: '§'},
                        {v: '—'},
                        {v: '`'},
                        {v: '…'},
                        {v: ','},
                        {v: '.'},
                        {v: 'SPACE', c: 3, i: 'glyphicon icon-space'},
                        {v: 'BACKSPACE', i: 'glyphicon icon-backspace'}
                    ]
                ]
            },
            numbers: {

            }
        }
    };

    $.fn.keyboard = function(type)  {
        var g = $('.keyboard').data('virtual-keyboard');
        if(!g) {
            g = new Keyboard();
            g.init();
        }
        return this.each(function() {
            var element = $(this);

            if (element.data('virtual-keyboard'))
                return;

            element.data('virtual-keyboard', g);
            element.data('virtual-keyboard-type', type || 'text');

            _gledoms.push(element);
            element.data('pos-index', _gledoms.length);

            element.focusin(function () {
                var k = element.data('virtual-keyboard');
                if(k) {
                    if(k.isEnabled()) {
                        k.setActiveDOM($(this));
                        k.setHidden(false);
                    }
                }
            })
        });
    };

})(jQuery);
